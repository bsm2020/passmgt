@extends('layouts.app')

@section('content')

        <div class="container py-3">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                        <div class="card">
                            <div class="card-header">
                                <h1>Edit Account Details</h1>
                            </div>

                            <div class="card-body">
                                <form method="POST" action="{{ route('account.update', $account->id) }}">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group">
                                        <label for="account">Account</label>
                                        <input type="text" name="account" class="form-control" value="{{ $account->account }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="description">Password</label>
                                        <input type="text" name="password" class="form-control" value="{{ $account->password }}">

                                    </div>

                                    <button type="submit" class="btn btn-primary">Update Account</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


@endsection

