@extends('layouts.app')

@section('content')
        <div class="container py-3">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <div class="card">
                        <div class="card-header">
                            <h1>{{ $account->account }}</h1>
                        </div>

                        <div class="card-body">
                            <p>{{ $account->password }}</p>

                            <a href="{{ route('account.edit', $account->id) }}" class="btn btn-primary">Edit Accountt</a>
                            <form action="{{ route('account.destroy', $account->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger mt-3">Delete</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

