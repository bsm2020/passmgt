@extends('layouts.app')

@section('content')
        <div class="container py-3">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <form method="POST" action="{{ route('account.store') }}">
                        @csrf

                        <div class="form-group">
                            <label for="account">Site Account</label>
                            <input type="text" name="account" class="form-control" value="{{ old('title') }}"
                                placeholder="Account Name">
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="text" name="password" class="form-control" value="{{ old('password') }}"
                                placeholder="Account Password">
                        </div>

                        <button type="submit" class="btn btn-primary">Save Account Details</button>
                    </form>
                </div>
            </div>
        </div>
 @endsection
