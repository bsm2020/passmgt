@extends('layouts.app')

@section('content')
<div class="container py-3">
          <div class="row">
            @foreach(  $accounts as $account)
              <div class="col-md-4">
                  <div class="card">
                      <div class="card-header">
                          <h3>{{ $account->account }}</h3>
                      </div>
                      <div class="card-body">
                          <p>{{ substr($account->password, 0, 100) }}</p>
                          <a href="{{ route('account.show', $account->id) }}" class="btn btn-primary btn-block">Read More</a>
                      </div>
                  </div>
              </div>
              @endforeach
          </div>
      </div>
@endsection
