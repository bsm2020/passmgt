<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    //
    protected $fillable=['account','password'];


    //Relationship between user table and budget table
    public function user()
    {

        return $this->belongsTo('App\Models\User');
    }



}
